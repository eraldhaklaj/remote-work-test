$(document).ready(() => {
    $("#add_product_extra").click(e => {
        e.preventDefault();

        // This will the number on the last element. The number on the last element is taken.
        // Because when we remove some options, I did not update the existing numbers.
        // Therefore if the third element is removed, the fourth element will still have the number 4 as index.
        // Due to that, on the new element I need to put 5 as the index or there will be problems adding click events/handlers.
        let extra_options_no; 
        
        if($(".extra-options").children().length > 0) {
            extra_options_no = parseInt($(".extra-options").children().last().attr('data-index'));
        }
        else {
            extra_options_no = 0;
        }


        $(".extra-options").append(`
            <div class="extra-option mb-4" data-index="${extra_options_no+1}">
                <div class="row">
                    <div class="col-5">
                        <label for="extra_option_label_${extra_options_no+1}">Option</label>
                        <input type="text" name="extra_option_label_${extra_options_no+1}" class="form-control" id="extra_option_label_${extra_options_no+1}" placeholder="Eg. Size" required>
                    </div>
                    <div class="col-7">
                        <div class="row">
                            <div class="col-9">
                                <div id="extra_option_values_${extra_options_no+1}">
                                    <div class="mb-3" data-index="1">
                                        <label for="extra_option_value_${extra_options_no+1}_1">Value</label>
                                        <input type="text" name="extra_option_value_${extra_options_no+1}_1" class="form-control" id="extra_option_value_${extra_options_no+1}_1" placeholder="Eg. M" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3" id="extra_option_actions_${extra_options_no+1}">
                                <div class="mb-3">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-danger w-100" id="remove_extra_option_${extra_options_no+1}_1"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-info" id="add_extra_option_${extra_options_no+1}"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `);

        $(`#add_extra_option_${extra_options_no+1}`).click(e => {
            e.preventDefault();

            // This will never return 0, because if there are no children, I remove the whole option.
            let extra_option_values_no = parseInt($(`#extra_option_values_${extra_options_no+1}`).children().last().attr('data-index'));

            $(`#extra_option_values_${extra_options_no+1}`).append(`
                <div class="mb-3" data-index="${extra_option_values_no+1}">
                    <label for="extra_option_value_${extra_options_no+1}_${extra_option_values_no+1}">Value</label>
                    <input type="text" name="extra_option_value_${extra_options_no+1}_${extra_option_values_no+1}" class="form-control" id="extra_option_value_${extra_options_no+1}_${extra_option_values_no+1}" placeholder="Eg. M" required>
                </div>
            `);

            $(`#extra_option_actions_${extra_options_no+1}`).append(`
                <div class="mb-3">
                    <label>&nbsp;</label>
                    <button class="btn btn-danger w-100" id="remove_extra_option_${extra_options_no+1}_${extra_option_values_no+1}"><i class="fas fa-minus"></i></button>
                </div>
            `);

            $(`#remove_extra_option_${extra_options_no+1}_${extra_option_values_no+1}`).click(e => {
                e.preventDefault();
                const remove_button = $(`#remove_extra_option_${extra_options_no+1}_${extra_option_values_no+1}`).parent();
                const item_index = $(`#extra_option_actions_${extra_options_no+1}`).children().index(remove_button);
                const extra_option_values_container = $(`#extra_option_values_${extra_options_no+1}`);

                $(`#extra_option_values_${extra_options_no+1}`).children().eq(item_index).remove();
                remove_button.remove();
                
                if(extra_option_values_container.children().length < 1) {
                    extra_option_values_container.parents('.extra-option').remove();
                }
            });
        });

        $(`#remove_extra_option_${extra_options_no+1}_1`).click(e => {
            e.preventDefault();
            const remove_button = $(`#remove_extra_option_${extra_options_no+1}_1`).parent();
            const item_index = $(`#extra_option_actions_${extra_options_no+1}`).children().index(remove_button);
            const extra_option_values_container = $(`#extra_option_values_${extra_options_no+1}`);

            $(`#extra_option_values_${extra_options_no+1}`).children().eq(item_index).remove();
            remove_button.remove();
            
            if(extra_option_values_container.children().length < 1) {
                extra_option_values_container.parents('.extra-option').remove();
            }
        });

    });

    
    function getTriangles(points) {
        let result = [];
        for(let x = 0; x<points.length; x++) {
            for(let y = x+1; y<points.length; y++) {
                for(let z = y+1; z<points.length; z++) {
                    result.push([points[x], points[y], points[z]]);
                }
            }
        }
        return result;
    }


    $("#product_form").on('submit', e => {
        e.preventDefault();

        const product_name = $("#name").val();
        const product_description = $("#description").val();
        const product_price = $("#price").val();
        const product_news = $("#news").prop('checked') ? "true" : "false";

        let options = {};
        $(".extra-option").each(function() {
            $this = $(this);
            let optionKey = $this.find("[name^='extra_option_label_']").val();
            options[optionKey] = [];
            $this.find("[id^='extra_option_value_']").each(function() {
                let $this = $(this);
                options[optionKey].push(`${optionKey} ${$this.val()}`);
            });
        });

        let option_values = [...Object.values(options)];
        
        let variants = option_values.reduce((a, b) => {
            return a.reduce((r, v) => {
                return r.concat(
                    b.map(w => {
                        console.log("smth");
                        return [].concat(v, w) // This returns all the possible variants from the given options
                    })
                )
            }, [])
        })
        .map(variant => {
            return variant.join(', ') + ' title: ' + product_name + ', description: ' + product_description + ', price: € ' + product_price + ', news: ' + product_news;
        })

        let product = {
            product_name,
            product_description,
            product_price,
            product_news,
            variants
        };

        console.log(product);

        $.ajax({
            url: "https://endpoint.com",
            data: product,
            success: function( res ) {
                console.log(res);
            }
        });
    })
});